
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct Image {
    pub Created: u64,
    pub Id: String,
    pub ParentId: String,
    pub RepoTags: Vec<String>,
    pub Size: u64,
    pub VirtualSize: u64,
}


#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct HealthCheck {
    pub Test: Vec<String>, 
    pub Interval: u64, 
    pub Timeout: u64, 
    pub Retries: u64,
    pub StartPeriod: u64,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct ContainerConfig {
    pub Hostname: String,
    pub Domainname: String, 
    pub User: String,
    pub AttachStdin: bool, 
    pub AttachStdout: bool,
    pub AttachStderr: bool,
    pub ExposedPorts: Option<HashMap<String, HashMap<i32, i32>>>,
    pub Tty: bool,
    pub OpenStdin: bool, 
    pub StdinOnce: bool, 
    pub Env: Vec<String>, 
    pub Cmd: Option<Vec<String>>,
    pub HealthCheck: Option<HealthCheck>,
    pub ArgsEscaped: Option<bool>,
    pub Image: String,
    pub Volumes: Option<HashMap<String, HashMap<String, String>>>,
    pub WorkingDir: String, 
    pub Entrypoint: Vec<String>,
    pub NetworkDisabled: Option<bool>,
    pub MacAddress: Option<String>, 
    pub OnBuild: Option<Vec<Option<String>>>,
    pub Labels: Option<HashMap<String, String>>,
    pub StopSignal: Option<String>, 
    pub StopTimeout: Option<u64>, 
    pub Shell: Option<Vec<String>>,
}


#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct GraphDriver{
    pub Name: String,
    pub Data: HashMap<String, String>,
}


#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct RootFs{
    pub Type: String,
    pub Layers: Vec<String>,
}


#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct Metadata{
    pub LastTagTime: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct ImageInspect {
    pub Id: String,
    pub RepoTags: Vec<String>,
    pub RepoDigests: Vec<String>,
    pub Parent: String,
    pub Comment: String,
    pub Created: String,
    pub Container: String,
    pub ContainerConfig: ContainerConfig,
    pub DockerVersion: String, 
    pub Author: String,
    pub Config: ContainerConfig,
    pub Architecture: String,
    pub Variant: Option<String>,
    pub Os: String,
    pub OsVersion: Option<String>,
    pub Size: u64,
    pub VirtualSize: u64,
    pub GraphDriver: GraphDriver,
    pub RootFs: Option<RootFs>,
    pub Metadata: Metadata,
}



impl Clone for Image {
    fn clone(&self) -> Self {
        let image = Image {
            Created: self.Created,
            Id: self.Id.clone(),
            ParentId: self.ParentId.clone(),
            RepoTags: self.RepoTags.clone(),
            Size: self.Size,
            VirtualSize: self.VirtualSize,
        };
        return image;
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ImageStatus {
    pub status: Option<String>,
    pub error: Option<String>,
}

impl Clone for ImageStatus {
    fn clone(&self) -> Self {
        let image_status = ImageStatus {
            status: self.status.clone(),
            error: self.status.clone(),
        };
        return image_status;
    }
}
